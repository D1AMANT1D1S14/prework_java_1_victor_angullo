function miFuncionDeClasificar(numero1) {
    // Recuerda completar los condicionales 'if'

    if (numero1 <= 10) {
        console.log("es una rueda para un juguete pequeño");
    }

    if (numero1 > 10 < 20) {
        console.log("es una rueda para un juguete mediano");
    }
    if (numero1 >= 20) {
        console.log("es una rueda para un jueguete grande");
    }
}

// Prueba con distintos números, por ejemplo con 5 y 2
miFuncionDeClasificar();

// Esto deberías ver por pantalla:
// "numero1 es estrictamente igual a 5"
// "numero2 es positivo"